﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;

namespace WordAddInAutomationSample
{
    [TestClass]
    public class AuthentificationTests
    {
        private static WindowsDriver<WindowsElement> _session;

        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";

        private const string WordAppPath = @"C:\Program Files\Microsoft Office\Office16\WINWORD.EXE";

        private const string WordFilePath = @"D:\2_5408993353435972345_20180802_125527330.docx";

        private const string DeviceName = "WindowsPC";

        private const string UserEmail = "UserEmail";

        private const string UserPassword = "UserPassword";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            if (_session == null)
            {
                DesiredCapabilities appCapabilities = new DesiredCapabilities();

                appCapabilities.SetCapability("app", WordAppPath);
                appCapabilities.SetCapability("appArguments", $"{WordFilePath} /q");
                appCapabilities.SetCapability("deviceName", DeviceName);

                _session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities);

                Assert.IsNotNull(_session);

                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
        }

        [TestMethod]
        public void TestMethod1()
        {
            _session.FindElementByName("DealCloud").Click();
            _session.FindElementByName("DealCloud Login").Click();

            CloseSecurityAlert();

            EnterText("Email", UserEmail);
            EnterText("Password", UserPassword);

            _session.FindElementByName("Log in").Click();

            Thread.Sleep(TimeSpan.FromSeconds(5));

            CloseSecurityAlert();

            Thread.Sleep(TimeSpan.FromSeconds(5));
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            if (_session != null)
            {
                _session.Quit();
                _session = null;
            }
        }

        public void CloseSecurityAlert()
        {
            _session.Keyboard.SendKeys(Keys.Alt + "y");
        }

        public void EnterText(string elementName, string text)
        {
            var element = _session.FindElementByName(elementName);

            element.Click();
            element.SendKeys(text);

            element.SendKeys(Keys.Control + "a");
            element.SendKeys(Keys.Backspace);
            
            _session.Keyboard.SendKeys(text);
        }
    }
}
